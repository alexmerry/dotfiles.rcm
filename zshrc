#if [ -f ~/.zshrc.grml ]; then
#  source ~/.zshrc.grml
#fi

export ZSH=$HOME/.oh-my-zsh

export EDITOR=vim
export VISUAL=$EDITOR
export WORKON_HOME=$HOME/.virtualenvs

export GOPATH=$HOME/src/go

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="rg3"

# Uncomment the following line to use case-sensitive completion.
CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="yyyy-mm-dd"

# Would you like to use another custom folder than $ZSH/custom?
ZSH_CUSTOM=$HOME/.zsh/oh-my-zsh

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
    profiles
    colored-man-pages
    copydir
    copyfile
    gitfast
    gpg-agent
    virtualenvwrapper
    #vi-mode # seems to interfere with home/end/up/down key bindings
    home-bin
    )

# Local config
[[ -f ~/.zshrc.local-opts ]] && source ~/.zshrc.local-opts

source $ZSH/oh-my-zsh.sh

# Balk on globs with no matches
setopt nomatch

# Don't share history between active sessions by default
setopt noshare_history

unsetopt AUTO_CD

# Local config
[[ -f ~/.zshrc.local ]] && source ~/.zshrc.local

if ! which go >/dev/null; then
    export PATH=$PATH:$(go env GOPATH)/bin
fi

# Start the shell with $? set to 0, even in .zshrc.local doesn't exist
true
