# Switch to kde-devel user
function go-kde()
{
    xauth list $DISPLAY | sed "s/^.*$DISPLAY/$DISPLAY/" > /tmp/x.auth
    nice ionice -c3 su - kde-devel
    rm -f /tmp/x.auth
}
