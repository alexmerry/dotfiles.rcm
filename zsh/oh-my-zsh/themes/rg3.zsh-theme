# Copy and self modified from ys.zsh-theme, the one of default themes in master repository
# Clean, simple, compatible and meaningful.
# Tested on Linux, Unix and Windows under ANSI colors.
# It is recommended to use with a dark background and the font Inconsolata.
# Colors: black, red, green, yellow, *blue, magenta, cyan, and white.
# http://xiaofan.at
# 2 Jul 2015 - Xiaofan

# Machine name.
function box_name {
    [ -f ~/.box-name ] && cat ~/.box-name || echo $HOST
}

# Directory info.
local current_dir='${PWD/#$HOME/~}'

# Git info.
local git_info='$(git_prompt_info)'
local git_status='$(git_prompt_status)'
local git_last_commit='$(git log --pretty=format:"%h \"%s\"" -1 2> /dev/null)'

ZSH_THEME_GIT_PROMPT_ADDED="+"
ZSH_THEME_GIT_PROMPT_AHEAD="↑"
ZSH_THEME_GIT_PROMPT_BEHIND="↓"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[green]%}✔︎"
ZSH_THEME_GIT_PROMPT_DELETED="×"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[red]%}✗"
ZSH_THEME_GIT_PROMPT_DIVERGED="%{$fg_bold[red]%}→←%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_MODIFIED="*"
ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[white]%}on%{$reset_color%} %{$fg_bold[cyan]%}"
ZSH_THEME_GIT_PROMPT_RENAMED="➜"
ZSH_THEME_GIT_PROMPT_STASHED="%{$fg[yellow]%}S%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_UNMERGED="%{$fg_bold[red]%}U%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_UNTRACKED="?"

if [[ -n $SSH_CONNECTION ]]; then
    HOSTNAME_COLOR="%{$fg_bold[green]%}"
else
    HOSTNAME_COLOR="%{$fg_no_bold[green]%}"
fi

# Prompt format: \n # TIME USER at MACHINE in [DIRECTORY] on BRANCH STATE \n $ 
PROMPT="
%(!.%{$fg[red]%}.%{$fg[cyan]%})%n \
%{$fg[white]%}at \
${HOSTNAME_COLOR}$(box_name) \
%{$fg[white]%}in \
%{$terminfo[bold]$fg[yellow]%}[${current_dir}]%{$reset_color%} \
${git_info} ${git_status} \
${git_last_commit}
%{$fg[red]%}%* \
%(?.%{$terminfo[bold]$fg[green]%}.%{$terminfo[bold]$fg[red]%})› %{$reset_color%}"

if [[ "$USER" == "root" ]]; then
PROMPT="
%{$fg[red]%}%* \
%{$terminfo[bold]$fg[blue]%}#%{$reset_color%} \
%{$bg[yellow]%}%{$fg[cyan]%}%n%{$reset_color%} \
%{$fg[white]%}at \
${HOSTNAME_COLOR}$(box_name) \
%{$fg[white]%}in \
%{$terminfo[bold]$fg[yellow]%}[${current_dir}]%{$reset_color%}\
%(?.%{$terminfo[bold]$fg[green]%}.%{$terminfo[bold]$fg[red]%})$ %{$reset_color%}"
fi
